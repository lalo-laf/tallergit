package aed;

import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;

class IntercalarTests {
    Intercalar intercalar = new Intercalar();

    @Test
    void testIntercalarStrings() throws FileNotFoundException {
        PrintStream salida = new PrintStream("intercalacion.txt");
        String p = "ab";
        String q = "cd";

        intercalar.imprimirStrings("", p, q, salida);
        Scanner archivoIntercalado = new Scanner(new File("intercalacion.txt"));
        assertTrue(archivoIntercalado.hasNextLine());
        assertEquals("abcd", archivoIntercalado.next());
        assertTrue(archivoIntercalado.hasNext());
        assertEquals("acbd", archivoIntercalado.next());
        assertTrue(archivoIntercalado.hasNext());
        assertEquals("acdb", archivoIntercalado.next());
        assertTrue(archivoIntercalado.hasNext());
        assertEquals("cabd", archivoIntercalado.next());
        assertTrue(archivoIntercalado.hasNext());
        assertEquals("cadb", archivoIntercalado.next());
        assertTrue(archivoIntercalado.hasNext());
        assertEquals("cdab", archivoIntercalado.next());
        assertFalse(archivoIntercalado.hasNext());
        archivoIntercalado.close();
    }
}


