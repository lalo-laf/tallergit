package aed;

public class Heap {
    private int tam;
    private int puntero;    //apunta a la proxima posicion a encolar
    private Router[] arr;

    public Heap(Router[] routers) { //O(n)
        tam = routers.length;
        puntero = tam;
        arr = new Router[tam];
        for (int i = 0; i < routers.length; i++) {  //hay aliasing
            arr[i] = routers[i];
        }
    }

    public void desencolar() {
        arr[0] = arr[puntero-1];
        arr[puntero-1] = null;
        bajar(0);
        puntero--;
    }

    public Router proximo() {
        return arr[0];
    }

    public void aplicarFloyd() {
        for (int i = tam-1; i >= 0; i--) {
            if(!esHoja(i)) {
                bajar(i);
            }
        }
    }


    public int padre(int i) {
        return (i-1)/2;
    }

    public int izq(int i) {
        return (2*i)+1;
    }

    public int der(int i) {
        return (2*i)+2;
    }

    public boolean esHoja(int i) {
        if (izq(i) >= tam) {//si ambos hijos "se salen" del array(si se sale izq, entonces se sale der)
            return true;
        }
        if (der(i) >= tam) {//si el hijo derecho "se sale" del array (solo podría haber hijo izq)
            return (arr[izq(i)] == null ? true : false);
        }
        else {              //si ningun hijo "se sale" del array
            return (arr[izq(i)] == null && arr[der(i)] == null ? true : false);
                            //creo que "&& arr[der(i)] == null" es innecesario
        }
    }

    public void swap(int a, int b) {//intercambia los nodos en posiciones a y b
        Router temp;
        temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }

    public void subir(int i) {//sube el nodo en posicion i hasta donde corresponda
        if(i != 0 && arr[i].getTrafico() > arr[padre(i)].getTrafico()) {
            swap(i, padre(i));
            subir(padre(i));
        }
    }

    public void bajar(int i) {//baja el nodo en posicion i hasta donde corresponda
        if (!esHoja(i)) {
            boolean unHijo = false;//sabemos que hay al menos 1 hijo, chequeamos si son 1 o 2
            if (der(i) >= tam) {
                unHijo = true;
            } else if (arr[der(i)] == null) {
                unHijo = true;
            }

            int max;//elijo el hijo a swapear, dependiendo de si tiene 1 o 2 hijos
            if (unHijo) {
                max = izq(i);
            } else {
                max = (arr[izq(i)].getTrafico() >= arr[der(i)].getTrafico() ? izq(i):der(i));
            }

            if (arr[max].getTrafico() > arr[i].getTrafico()) {//swapeamos, si corresponde
                swap(i,max);
                bajar(max);
            }
        }        
    }

    
}
