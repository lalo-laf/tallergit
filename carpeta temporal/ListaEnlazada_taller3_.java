package aed;

import java.util.*;

public class ListaEnlazada<T> implements Secuencia<T> {
    private Nodo _pri;
    private Nodo _ult;
    private int _size;
    private class Nodo {
        T valor;
        Nodo sig;
        Nodo ant;
        Nodo(T v) {
            valor = v;
            sig = null;
            ant = null;
        }
    }

    public ListaEnlazada() {
        _pri = null;
        _ult = null;
        _size = 0;
    }

    public int longitud() {
        return _size;
    }

    public void agregarAdelante(T elem) {
        Nodo nuevo = new Nodo(elem);
        if (_pri == null) {     //caso lista vacía
            _ult = nuevo;
        } else {
            _pri.ant = nuevo;
        }
        nuevo.sig = _pri;
        _pri = nuevo;
        _size++;
    }

    public void agregarAtras(T elem) {
        Nodo nuevo = new Nodo(elem);
        if (_pri == null) {     //caso lista vacía
            _pri = nuevo;
        } else {
            _ult.sig = nuevo;
        }
        nuevo.ant = _ult;
        _ult = nuevo;
        _size++;
    }

    public T obtener(int i) {
        Nodo actual = _pri;
        for (int j = 0; j < i; j++) {
            actual = actual.sig;
        }
        return actual.valor;
    }

    public void eliminar(int i) {
        Nodo nodoABorrar = _pri;
        Nodo nodoAnterior = _pri;
        for (int j = 0; j < i; j++) {
            nodoAnterior = nodoABorrar;
            nodoABorrar = nodoABorrar.sig;
        }
        if (_size == 1) {       //1 nodo
            _pri = null;
            _ult = null;
        } else {                //2 o mas nodos
        if (i == 0) {                   //caso primer nodo
            _pri = _pri.sig;
            nodoABorrar.sig.ant = null;                     //funciona igual sin el .sig.ant?
        } else if (i == _size-1) {      //caso ultimo nodo
            _ult = _ult.ant;
            nodoABorrar.ant.sig = null;                     //funciona igual sin el .ant.sig?
        } else {                        //caso nodo intermedio
            nodoAnterior.sig = nodoABorrar.sig;
            nodoABorrar.sig.ant = nodoAnterior;             //funciona igual sin el .sig.ant?
        }
        }
        _size--;
    }
    // p-(0)-(1)-(2)-x

    public void modificarPosicion(int indice, T elem) {
        Nodo actual = _pri;
        for (int i = 0; i < indice; i++) {
            actual = actual.sig;
        }
        actual.valor = elem;
    }

    public ListaEnlazada<T> copiar() {
        ListaEnlazada<T> nueva = new ListaEnlazada<T>();
        for (int i = 0; i < _size; i++) {
            nueva.agregarAtras(obtener(i));
        }
        return nueva;
    }

    public ListaEnlazada(ListaEnlazada<T> lista) {
        _pri = null;
        _ult = null;
        _size = 0;
        for (int i = 0; i < lista._size; i++) {
            agregarAtras(lista.obtener(i));
        }
    }
    
    @Override
    public String toString() {
        StringBuffer sbuffer = new StringBuffer();
        sbuffer.append("[");
        for (int i = 0; i < _size; i++) {
            sbuffer.append(obtener(i));
            if (i != _size-1) {
                sbuffer.append(", ");
            }
        }
        sbuffer.append("]");
        return sbuffer.toString();
    }

    private class ListaIterador implements Iterador<T> {
    	int _dedito;

        ListaIterador(){
            _dedito = 0;
        }

        public boolean haySiguiente() {
	        return _dedito < _size;
        }
        
        public boolean hayAnterior() {
	        return _dedito > 0;
        }

        public T siguiente() {
	        _dedito = _dedito + 1;
            return obtener(_dedito - 1);
        }

        public T anterior() {
            _dedito = _dedito - 1;
	        return obtener(_dedito);
        }
    }

    public Iterador<T> iterador() {
	    return new ListaIterador();
    }

}
