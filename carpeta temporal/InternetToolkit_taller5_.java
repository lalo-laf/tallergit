package aed;

public class InternetToolkit {
    public InternetToolkit() {
    }

    public Fragment[] tcpReorder(Fragment[] fragments) {
        for (int i = 1; i < fragments.length; i++) {
            if (fragments[i].compareTo(fragments[i-1]) < 0) {   //si es menor al anterior
                insertion(fragments, i);
            }
        }
        return fragments;
    }

    public Fragment[] insertion(Fragment[] fragments, int pos) {
        while (pos > 0 && fragments[pos].compareTo(fragments[pos-1]) < 0) {    //si es menor al anterior
            swap(fragments, pos, pos-1);
            pos = pos-1;
        }
        return fragments;
    }

    public Fragment[] swap(Fragment[] fragments, int pos1, int pos2) {
        Fragment temporal = fragments[pos1];
        fragments[pos1] = fragments[pos2];
        fragments[pos2] = temporal;
        return fragments;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public Router[] kTopRouters(Router[] routers, int k, int umbral) {
        Heap h = new Heap(routers);
        h.aplicarFloyd();
        Router[] res = new Router[k];
        for (int i = 0; i < res.length; i++) {
            if (h.proximo().getTrafico() > umbral) {
                res[i] = h.proximo();
                h.desencolar();
            }
            //h.desencolar();
        }
        return res;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public IPv4Address[] sortIPv4(String[] ipv4) {
        IPv4Address[] ips = new IPv4Address[ipv4.length];
        IPv4Address ip;
        for (int i = 0; i < ips.length; i++) {  //en ips tengo un array de ipv4's
            ip = new IPv4Address(ipv4[i]);
            ips[i] = ip;
        }

        for (int j = 3; j >= 0; j--) {
            for (int i = 1; i < ips.length; i++) {
                if (ips[i].getOctet(j) < ips[i-1].getOctet(j)) {
                    insertion2(ips, i, j);
                }
            }
        }
        return ips;
    }

    public IPv4Address[] insertion2(IPv4Address[] ips, int pos, int digito) {
        while (pos > 0 && ips[pos].getOctet(digito) < ips[pos-1].getOctet(digito)) {    //si es menor al anterior
            swap2(ips, pos, pos-1);
            pos = pos-1;
        }
        return ips;
    }

    public IPv4Address[] swap2(IPv4Address[] ips, int pos1, int pos2) {
        IPv4Address temporal = ips[pos1];
        ips[pos1] = ips[pos2];
        ips[pos2] = temporal;
        return ips;
    }

}
