package aed;

import java.util.*;

// Todos los tipos de datos "Comparables" tienen el método compareTo()
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    private Nodo _raiz;
    private int _cardinal;

    private class Nodo {
        T valor;
        Nodo izq;
        Nodo der;
        Nodo padre;

        Nodo (T v) {
            valor = v;
            izq = null;
            der = null;
            padre = null;
        }
    }

    public ABB() {
        _raiz = null;
        _cardinal = 0;
    }

    public int cardinal() {
        return _cardinal;
    }

    public T minimo(){
        Nodo actual = _raiz;
        while (actual.izq != null) {
            actual = actual.izq;
        }
        return actual.valor;
    }

    private Nodo minNodo(Nodo n) {
        Nodo actual = n;
        while (actual.izq != null) {
            actual = actual.izq;
        }
        return actual;
    }

    public T maximo(){
        Nodo actual = _raiz;
        while (actual.der != null) {
            actual = actual.der;
        }
        return actual.valor;
    }

    public void insertar(T elem){
        if (pertenece(elem)) return;

        Nodo actual = _raiz;
        Nodo anterior = _raiz;
        while (actual != null) {
            if (elem.compareTo(actual.valor) > 0) {
                anterior = actual;
                actual = actual.der;
            } else {
                anterior = actual;
                actual = actual.izq;
            }
        }

        Nodo nuevo = new Nodo(elem);
        nuevo.padre = anterior;
        if (_raiz != null) {
            if (elem.compareTo(anterior.valor) > 0) {   //va a la derecha
                anterior.der = nuevo;
            } else {                                    //va a la izquierda
                anterior.izq = nuevo;
            }
        } else {
            _raiz = nuevo;
        }
        _cardinal++;
    }

    public boolean pertenece(T elem){
        return aux(_raiz, elem);
    }

    private boolean aux(Nodo a, T elem) {
        if (a == null) return false;
        if (elem.compareTo(a.valor) == 0) return true;
        if (elem.compareTo(a.valor) > 0) {
            return aux(a.der, elem);
        } else {
            return aux(a.izq, elem);
        }
    }

    public void eliminar(T elem){
        if (!pertenece(elem)) return;
        
        Nodo actual = _raiz;
        while (elem.compareTo(actual.valor) != 0) {
            if (elem.compareTo(actual.valor) > 0) {
                actual = actual.der;
            } else {
                actual = actual.izq;
            }
        }   //aca actual apunta al nodo a eliminar

        if(actual.der == null && actual.izq == null) {              //CASO 0 HIJOS
            if(actual == _raiz) {//caso borrar raiz
                _raiz = null;
            } else if(elem.compareTo(actual.padre.valor) > 0) {
                actual.padre.der = null;
            } else {
                actual.padre.izq = null;
            }
            _cardinal--;
        } else if(actual.der == null || actual.izq == null) {              //CASO 1 HIJO
            Nodo hijo = actual.der==null?actual.izq:actual.der;
            hijo.padre = actual.padre;
            if(actual == _raiz) {//caso borrar raiz
                _raiz = hijo;
            } else if(hijo.valor.compareTo(actual.padre.valor) > 0) {   //el nuevo hijo es mayor
                actual.padre.der = hijo;
            } else {                                                    //el nuevo hijo es menor
                actual.padre.izq = hijo;
            }
            _cardinal--;
        } else if(actual.der != null && actual.izq != null) {              //CASO 2 HIJOS
            T temporal = sucesor(elem).valor;
            eliminar(sucesor(elem).valor);
            actual.valor = temporal;
        }
    }

    private Nodo sucesor(T elem) {
        Nodo actual = _raiz;
        Nodo posible_suc = null;
        while (elem.compareTo(actual.valor) != 0) {  //tambien: actual.valor != elem?
            if (actual.valor.compareTo(elem) > 0) posible_suc = actual;
            if (elem.compareTo(actual.valor) > 0) {
                actual = actual.der;
            } else if (elem.compareTo(actual.valor) < 0) {
                actual = actual.izq;
            }
        }   //aca actual apunta al nodo elem
        if(actual.der != null) {
            return minNodo(actual.der);
        } else {
            return posible_suc;
        }
    }

    public String toString(){
        StringBuffer sBuffer = new StringBuffer();
        sBuffer.append("{");
        Iterador<T> it = iterador();
        while (it.haySiguiente()) {
            sBuffer.append(it.siguiente());
            if(it.haySiguiente()) sBuffer.append(",");
        }
        sBuffer.append("}");
        return sBuffer.toString();
    }

    private class ABB_Iterador implements Iterador<T> {
        private Nodo _actual;

        ABB_Iterador() {
            _actual = _raiz;
            while (_actual.izq != null) {
                _actual = _actual.izq;
            }
        }

        public boolean haySiguiente() {            
            return _actual != null;
        }
    
        public T siguiente() {
            T temporal = _actual.valor;
            _actual = sucesor(_actual.valor);
            return temporal;
        }
    }

    public Iterador<T> iterador() {
        return new ABB_Iterador();
    }

}
